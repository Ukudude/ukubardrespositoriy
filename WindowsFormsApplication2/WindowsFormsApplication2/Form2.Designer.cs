﻿namespace WindowsFormsApplication2
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.TreeNode treeNode1 = new System.Windows.Forms.TreeNode("Иванов");
            System.Windows.Forms.TreeNode treeNode2 = new System.Windows.Forms.TreeNode("Петров");
            System.Windows.Forms.TreeNode treeNode3 = new System.Windows.Forms.TreeNode("Сидоров");
            System.Windows.Forms.TreeNode treeNode4 = new System.Windows.Forms.TreeNode("Группа 7095", new System.Windows.Forms.TreeNode[] {
            treeNode1,
            treeNode2,
            treeNode3});
            System.Windows.Forms.TreeNode treeNode5 = new System.Windows.Forms.TreeNode("Никифоров");
            System.Windows.Forms.TreeNode treeNode6 = new System.Windows.Forms.TreeNode("Карабаев");
            System.Windows.Forms.TreeNode treeNode7 = new System.Windows.Forms.TreeNode("Лобанов");
            System.Windows.Forms.TreeNode treeNode8 = new System.Windows.Forms.TreeNode("Группа 7097", new System.Windows.Forms.TreeNode[] {
            treeNode5,
            treeNode6,
            treeNode7});
            this.treeView1 = new System.Windows.Forms.TreeView();
            this.SuspendLayout();
            // 
            // treeView1
            // 
            this.treeView1.Location = new System.Drawing.Point(13, 13);
            this.treeView1.Name = "treeView1";
            treeNode1.Name = "Иванов";
            treeNode1.Text = "Иванов";
            treeNode2.Name = "Петров";
            treeNode2.Text = "Петров";
            treeNode3.Name = "Сидоров";
            treeNode3.Text = "Сидоров";
            treeNode4.Name = "Группа 7095";
            treeNode4.Text = "Группа 7095";
            treeNode5.Name = "Узел5";
            treeNode5.Text = "Никифоров";
            treeNode6.Name = "Карабаев";
            treeNode6.Text = "Карабаев";
            treeNode7.Name = "Лобанов";
            treeNode7.Text = "Лобанов";
            treeNode8.Name = "Группа 7097";
            treeNode8.Text = "Группа 7097";
            this.treeView1.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            treeNode4,
            treeNode8});
            this.treeView1.Size = new System.Drawing.Size(121, 97);
            this.treeView1.TabIndex = 0;
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.treeView1);
            this.Name = "Form2";
            this.Text = "Form2";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TreeView treeView1;
    }
}